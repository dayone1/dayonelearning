import Head from 'next/head';

const siteName = 'Day One';
const title = 'Day One | Practical Learning from Day One';
const description =
  'Learn web programming by being engaged in the actual building experience. We are by your side to to ensure that learning happens.';

const DefaultHead = () => (
  <Head>
    <title key='title'>{title}</title>

    <link rel='shortcut icon' type='image/x-icon' href='/favicon.ico' />

    <link
      rel='apple-touch-icon'
      sizes='180x180'
      href='/images/apple-touch-icon.png'
    />
    <link
      rel='icon'
      type='image/png'
      sizes='32x32'
      href='/images/favicon-32x32.png'
    />
    <link
      rel='icon'
      type='image/png'
      sizes='16x16'
      href='/images/favicon-16x16.png'
    />
    <link rel='manifest' href='/site.webmanifest'></link>

    <meta key='description' name='description' content={description} />
    <meta key='og-url' property='og:url' content='https://dayonelearning.com' />
    <meta key='og-type' property='og:type' content='website' />
    <meta key='og-title' property='og:title' content={title} />
    <meta
      key='og-description'
      property='og:description'
      content={description}
    />
    <meta key='og-site_name' property='og:site_name' content={siteName} />
  </Head>
);

export default React.memo(DefaultHead);
