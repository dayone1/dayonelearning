import styles from './content-card.module.css';

const moreText = `
اطلاعات بیش‌تر
`;

const ContentCard = ({
  centered = false,
  id,
  image,
  children,
  nextText = moreText,
  nextHref,
  iframeVideo,
}) => (
  <div id={id || ''} className={styles.pageContainer}>
    <div className={styles.cardContainer}>
      <div
        className={`${styles.textContainer} ${centered ? styles.centered : ''}`}
      >
        {children}
      </div>
      {image && (
        <div className={styles.imageContainer}>
          <img src={image.src} alt={image.alt} />
        </div>
      )}
      {iframeVideo && (
        <div className={styles.iframeContainer}>
          <iframe
            src={iframeVideo}
            allowFullScreen='true'
            allowFullScreen='allowfullscreen'
            webkitallowfullscreen='true'
            mozallowfullscreen='true'
          ></iframe>
        </div>
      )}
    </div>
    {nextHref && (
      <a href={nextHref} className={styles.nextButton}>
        {nextText}
      </a>
    )}
  </div>
);

export default React.memo(ContentCard);
