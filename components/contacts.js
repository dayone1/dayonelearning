import ContentCard from './content-card';

const contactText = `تماس با ما`;

const Contacts = (props) => (
  <ContentCard {...props}>
    <h3>{contactText}</h3>
    <br />
    <p>
      اگر سوالی دارید یا می‌خواهید بیش‌تر در مورد ما بدانید با ما تماس بگیرید.
    </p>
    <br />
    <p>تلگرام:</p>
    <a href='https://t.me/day_one_learning' dir='ltr'>
      @day_one_learning
    </a>
    <p>ای‌میل:</p>
    <a href='mailto:salam@dayonelearning.com'>salam@dayonelearning.com</a>
  </ContentCard>
);

export default React.memo(Contacts);
