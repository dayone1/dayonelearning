import ContentCard from './content-card';

const videoHeadingText = `کلاس‌ها چه طور برگزار می‌شوند؟`;

const VideoCard = (props) => (
  <ContentCard
    {...props}
    iframeVideo='https://www.aparat.com/video/video/embed/videohash/VfHEO/vt/frame'
  >
    <h3>{videoHeadingText}</h3>
    <br />
    <p>در ویدیوی زیر ببینید:</p>
  </ContentCard>
);

export default VideoCard;
