import Link from 'next/link';
import ContentCard from './content-card';

const valuePropositionText = `
یادگیری برنامه‌نویسی به روش عملی
`;

const subscribeText = 'ثبت‌نام';

const programStartText = `
شروع دوره: ۱۲ مرداد
`;

const programLengthText = `
۱۰ هفته. هر هفته یک جلسه.
`;

const onlineText = `
نحوه برگزاری کلاس‌ها: آنلاین
`;

const priceText = `
هزینه‌ی دوره: هر جلسه ۱۲۰ هزار تومان
`;

const guaranteeText = `
ثبت نام تنها با پرداخت هزینه‌ی اولین جلسه.
اگر به هر دلیلی از ادامه دادن دوره منصرف شدید، نصف این مبلغ نیز دوباره به شما بازگردانده می‌شود.
`;

const ProgramDescription = (props) => {
  return (
    <ContentCard
      centered
      image={{ src: 'images/banner.jpg', alt: 'image' }}
      {...props}
    >
      <h3>{valuePropositionText}</h3>
      <p>{programStartText}</p>
      <p>{programLengthText}</p>
      <p>{onlineText}</p>
      <p>{priceText}</p>
      <br />
      <p>{guaranteeText}</p>
      <br />
      <a href='https://Zarinp.al/321494' className='sharp-button'>
        {subscribeText}
      </a>
    </ContentCard>
  );
};

export default React.memo(ProgramDescription);
