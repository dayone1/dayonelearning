import ContentCard from './content-card';
import teachers from './../data/teachers';

const MAX_CHARS = 200;

const expandText = `
مشاهده بقیه متن
`;

const shrinkText = `
کوتاه کردن متن
`;

const selectTeacherReason = (reason, shouldExpand) =>
  shouldExpand ? reason : reason.substring(0, MAX_CHARS) + '...';

const TeacherInfo = ({ teacher, ...rest }) => {
  const [shouldExpand, setShouldExpand] = React.useState(false);

  const toggleExpand = React.useCallback(() => {
    setShouldExpand((old) => !old);
  }, []);

  return (
    <ContentCard {...rest} image={teachers[teacher].image}>
      <h3>{teachers[teacher].name}</h3>
      {teachers[teacher].items.map((item) => (
        <p key={item} className='text-small'>
          {item}
        </p>
      ))}
      <p>{selectTeacherReason(teachers[teacher].reason, shouldExpand)}</p>
      <button type='button' className='expand-button' onClick={toggleExpand}>
        {!shouldExpand ? expandText : shrinkText}
      </button>
    </ContentCard>
  );
};

export default React.memo(TeacherInfo);
