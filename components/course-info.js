import ContentCard from './content-card';

const goToTeachersText = `
چه کسانی درس می‌دهند؟
`;

const courseInfoHeading = `
ویژگی‌های دوره
`;

const CourseInfo = (props) => (
  <ContentCard nextText={goToTeachersText} {...props}>
    <h3>{courseInfoHeading}</h3>
    <p>
      در این جلسه به کمک مدرسین شما از همان جلسه‌ی اول به صورت عملی برنامه‌نویسی
      وب را آغاز می‌کنید. به طوری که در پایان اولین روز وب سایت خودتان را
      ساخته‌اید، بی‌آنکه زمان زیادی را صرف گذراندن مباحث تئوری کرده باشید. طی
      جلسات بعدی، یاد می‌گیرید که چه‌طور ویژگی‌های مختلفی به آن اضافه کنید. در
      نهایت در طی ده جلسه خروجی کار وب‌سایتی است که آن را خودتان ساخته‌اید.
    </p>
    <p>
      وب سایتی که در این ده جلسه آماده خواهید کرد، فضایی است برای ساختن
      دسته‌بندی‌های متفاوت از ویدیوهای موجود در سایت آپارات. کاربران وب سایت شما
      خواهند توانست ویدیوهای آپارات را دست‌چین و مطابق سلیقه‌شان دسته‌بندی کنند.
      مثلا دسته‌بندی بر اساس فیلم‌های مستند یا موزیک‌های شاد.
    </p>
    <br />
    {/* <p>
      اطلاعات بیش‌تر را می‌توانید <a href='#'>این‌جا</a> ببینید.
    </p> */}
  </ContentCard>
);

export default React.memo(CourseInfo);
