import DefaultHead from './../components/default-head';
import './../global-styles.css';

// config.autoAddCss = false;

function MyApp({ Component, pageProps }) {
  return (
    <>
      <DefaultHead />
      <Component {...pageProps} />
    </>
  );
}

export default MyApp;
