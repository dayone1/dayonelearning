import Link from 'next/link';
import ContentCard from './../../components/content-card';
import { logPageView, initGA } from './../../utils/google-analytics';

const failText = `
مشکلی در پرداخت به وجود آمد.
`;

const supportText = `
اگر خودتان پرداخت را لغو نکردید و فکر می‌کنید مشکلی پیش آمده 
لطفا با ما تماس بگیرید تا پی‌گیر حل مشکل‌تان باشیم.
`;

const goBackText = `
برگشت به صفحه اول
`;

const telegramText = `
روی تلگرام: 
`;

const PaymentFailPage = () => {
  React.useEffect(() => {
    if (window && !window.GA_INITIALIZED) {
      initGA();
      window.GA_INITIALIZED = true;
    }
    logPageView();
  }, []);
  return (
    <ContentCard>
      <p>{failText}</p>
      <p>{supportText}</p>
      <br />
      <br />

      <a href='mailto:salam@dayonelearning.com'>salam@dayonelearning.com</a>
      <br />
      <p>{telegramText}</p>
      <a href='https://t.me/day_one_learning' dir='ltr'>
        @day_one_learning
      </a>

      <Link href='/'>
        <a className='bottom-button'>{goBackText}</a>
      </Link>
    </ContentCard>
  );
};

export default PaymentFailPage;
