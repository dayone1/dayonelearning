import Link from 'next/link';
import ContentCard from './../../components/content-card';
import { logPageView, initGA } from './../../utils/google-analytics';

const thanksText = `
از خریدتان سپاسگزاریم.
`;

const followUptext = `
به زودی برای هماهنگی با شما تماس می‌گیریم.
`;

const goBackText = `
برگشت به صفحه اول
`;

const PaymentSuccessPage = () => {
  React.useEffect(() => {
    if (window && !window.GA_INITIALIZED) {
      initGA();
      window.GA_INITIALIZED = true;
    }
    logPageView();
  }, []);
  return (
    <ContentCard>
      <p>{thanksText}</p>
      <p>{followUptext}</p>
      <Link href='/'>
        <a className='bottom-button'>{goBackText}</a>
      </Link>
    </ContentCard>
  );
};

export default PaymentSuccessPage;
