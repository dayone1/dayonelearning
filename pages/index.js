import ProgramDescription from '../components/program-description';
import CourseInfo from '../components/course-info';
import TeacherInfo from '../components/teacher-info';
import Contacts from '../components/contacts';
import VideoCard from '../components/video-card';
import { logPageView, initGA } from '../utils/google-analytics';

const Home = () => {
  React.useEffect(() => {
    if (window && !window.GA_INITIALIZED) {
      initGA();
      window.GA_INITIALIZED = true;
    }
    logPageView();
  }, []);

  return (
    <>
      <ProgramDescription
        id='program-description'
        nextHref='#video'
        nextText='کلاس‌ها چه شکلی برگزار می‌شوند؟'
      />
      <VideoCard id='video' nextHref='#course-info' nextHref='#course-info' />
      <CourseInfo id='course-info' nextHref='#first-teacher' />
      <TeacherInfo
        id='first-teacher'
        teacher='farid'
        nextHref='#mohammad'
        nextText='محمد رنجبر'
      />
      <TeacherInfo
        id='mohammad'
        teacher='mohammad'
        nextHref='#javad'
        nextText='جواد محمدیان'
      />
      <TeacherInfo
        id='javad'
        teacher='javad'
        nextHref='#program-description'
        nextText='می‌خواهم ثبت‌نام کنم'
      />
      <Contacts
        id='contacts'
        nextText='برگشت به بالا'
        nextHref='#program-description'
      />
    </>
  );
};

export default React.memo(Home);
