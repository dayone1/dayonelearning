import Cors from 'cors';
import runMiddleware from './../../../utils/run-middleware';

const cors = Cors({
  methods: ['GET'],
});

const handleReqs = async (req, res) => {
  try {
    await runMiddleware(req, res, cors);
    const {
      query: { video },
    } = req;
    const result = await fetch(
      `https://www.aparat.com/etc/api/video/videohash/${video}`
    );
    const jsonResult = await result.json();
    res.json(jsonResult);
  } catch (error) {
    res.status(400).json({ error });
  }
};

export default handleReqs;
