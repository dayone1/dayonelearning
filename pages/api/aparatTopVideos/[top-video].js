import Cors from 'cors';
import runMiddleware from '../../../utils/run-middleware';

const cors = Cors({
  methods: ['GET'],
});

const handleReqs = async (req, res) => {
  try {
    await runMiddleware(req, res, cors);
    const {
      query: { tag },
    } = req;
    const url = `https://www.aparat.com/etc/api/videobytag/text/sport`;
    console.log("featured-videos url", url);
    const result = await fetch(
      url
    );
    const jsonResult = await result.json();
    res.json(jsonResult.videobytag || []);
  } catch (error) {
    console.log("featured-videos error", error)
    res.status(400).json({ error });
  }
};

export default handleReqs;
