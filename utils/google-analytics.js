import ReactGA from 'react-ga';

const GOOGLE_ANALYTICS_TRACKING_ID = 'UA-173581542-1';

export const initGA = () => {
  ReactGA.initialize(GOOGLE_ANALYTICS_TRACKING_ID, {
    testMode: false,
  });
};

export const logPageView = () => {
  if (window) {
    ReactGA.set({ page: window.location.pathname });
    ReactGA.pageview(window.location.pathname);
  }
};

export const logEvent = (category = '', action = '') => {
  if (category && action) {
    ReactGA.event({ category, action });
  }
};

export const logException = (description = '', fatal = false) => {
  if (description) {
    ReactGA.exception({ description, fatal });
  }
};
